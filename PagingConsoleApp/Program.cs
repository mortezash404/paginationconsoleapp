﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PagingConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var persons = new List<Person>
            {
                new Person{Name = "Ali", Phone = "021"},
                new Person{Name = "Saeed", Phone = "022"},
                new Person{Name = "Hadi", Phone = "023"},
                new Person{Name = "Ahmad", Phone = "024"},
                new Person{Name = "Reza", Phone = "025"},
                new Person{Name = "Saber", Phone = "026"},
                new Person{Name = "Sasan", Phone = "027"},
                new Person{Name = "Ehsan", Phone = "028"},
                new Person{Name = "Akbar", Phone = "029"}
            };

            const int pageSize = 2;

            var pageResult = new
            {
                Count = persons.Count,
                Result = persons
            };

            var ceiling = Math.Ceiling(pageResult.Count / (double)pageSize);

            for (var i = 1; i <= ceiling; i++)
            {
                var paging = Paging(pageResult.Result, i, pageSize);

                foreach (var person in paging)
                {
                    Console.WriteLine($"page {i} data is : {person.Name}");
                }
            }
        }

        public static List<Person> Paging(List<Person> persons, int page, int pageSize)
        {
            return persons.Skip((page - 1) * pageSize).Take(pageSize).ToList();
        }
    }
}
